+++
title = "Στης Πάργας τον Ανήφορο"
date = "2017-09-19"
tags = [ "epirus" ]
topics = [ "" ]
+++

<div class="org-center">
<a href="https://picasaweb.google.com/lh/photo/KwL9WkmfJackEdGo_MEbUtMTjNZETYmyPJy0liipFm0?feat=embedwebsite"><img src="https://lh3.googleusercontent.com/-oIjEhWcDYw0/V53FKrCkMzI/AAAAAAAAD6A/gkkmUOuxfoY_WMpz674ZBCBdoUr8RrKSACCo/s640/Screen%2BShot%2B2016-07-31%2Bat%2B12.29.53.png" height="327" width="640" /></a>
</div>

<p class="verse">
Αχ στης Πάργας τον ανήφορο κανέλλα και γαρύφαλλο<br  />
Αχ με γέλασαν δυο παργανιές κοντούλες και μελαχρινές<br  />
<br  />
Αχ με γέλασε και μια μικρή κοντούλα και μελαχρινή<br  />
Αχ έλα μικρή, δεν έρχομαι, μεγάλωσα και ντρέπομαι<br  />
<br  />
Φέρε μας κρασί να πιούμε εγώ και συ<br  />
να πιούμε εγώ και συ αγάπη μου χρυσή<br  />
<br  />
Αχ με γέρασαν δυο πράγματα, σεβντάδες και γεράματα<br  />
Αχ με γέλασε και μια μικρή κοντούλα και μελαχρινή<br  />
</p>

<body><iframe width="250" height="205" src="http://www.youtube.com/embed/jnFo2KnxVzA" frameborder="0" allowfullscreen></iframe></body></center></div>
