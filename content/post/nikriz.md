+++
title = "Μακάμ Νικρίζ"
date = "2017-09-23"
tags = [ "makam", "theory", "dromoi", "modes", "nikriz" ]
topics = [ "" ]
+++

<a href='https://postimages.org/' target='_blank'><img src='https://s26.postimg.org/no8n586hl/nikriz.png' border='0' alt='nikriz'/></a>

**Links σχετικά με τον δρόμο Νικρίζ (του φίλου Μίτσου):**

-   [Λαϊκοί δρόμοι για κιθάρα-ΝΙΚΡΙΖ ΑΝΟΙΚΤΗ ΘΕΣΗ ](http://www.youtube.com/watch?v=B6xddWJFmt8&list=UUhH3EEsPGqjSwweLmSm4clw)
-   [oud.eclipse.co.uk/nikrizarab](http://www.oud.eclipse.co.uk/nikrizarab.html)
-   [Δρόμοι και Τρόποι - Μια συγκεντρωτική παρουσίαση](http://wiki.kithara.gr/%CE%94%CF%81%CF%8C%CE%BC%CE%BF%CE%B9_%CE%BA%CE%B1%CE%B9_%CE%A4%CF%81%CF%8C%CF%80%CE%BF%CE%B9_-_%CE%9C%CE%B9%CE%B1_%CF%83%CF%85%CE%B3%CE%BA%CE%B5%CE%BD%CF%84%CF%81%CF%89%CF%84%CE%B9%CE%BA%CE%AE_%CF%80%CE%B1%CF%81%CE%BF%CF%85%CF%83%CE%AF%CE%B1%CF%83%CE%B7)
