+++
title = "Την άμμο άμμο"
date = "2017-09-23"
tags = [ "syrtos_on_3", "5tonik_m", "epirus" ]
topics = [ "" ]
+++

<p class="verse">
Την άμμο άμμο η μαύρη εγώ την άμμο άμμο πήγαινα,<br  />
την άμμο άμμο πήγαινα τη θάλασσα(ν) αγνάντευα.<br  />
<br  />
Θάλασσα πι… η μαύρη εγώ θάλασσα πικροθάλασσα,<br  />
θάλασσα πικροθάλασσα τι μου ‘κανες τον άντρα μου.<br  />
<br  />
Τι μου ‘κανες η μαύρη εγώ τι μου ‘κανες τον άντρα μου,<br  />
τι μου ‘κανες τον  άντρα μου το φως μου και τα μάτια μου.<br  />
<br  />
Τον άντρα σου η μαύρη εγώ τον άντρα σου τον έπνιξα,<br  />
τον άντρα σου τον έπνιξα και στα βαθιά τον έριξα.<br  />
<br  />
Που να βρω τώρα η μαύρη εγώ που να βρω κολυμπητή,<br  />
που να βρω εγώ κολυμπητή τον άντρα μου να πάει να βρει.<br  />
</p>

<body><iframe width="250" height="205" src="http://www.youtube.com/embed/_MMk0pX4Dts" frameborder="0" allowfullscreen></iframe></body></center></div>
