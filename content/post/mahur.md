+++
title = "Μακάμ Mahur"
date = "2017-09-23"
tags = [ "makam", "dromoi", "modes", "mahur" ]
topics = [ "" ]
+++

<a href="http://imgur.com/yBd1f5A"><img src="http://i.imgur.com/yBd1f5A.jpg?1" title="source: imgur.com" /></a>

Άρχισε ακούγοντας το *Mahur Saz Semai - Kemenceci Nikolaki* παιγμένο απο τον Σωκράτη Σινόπουλο, τον Ara Dinkjian και άλλους παιχταράδες. Με μάγεψε ο ήχος του μακάμ αυτού. Το ερωτεύτηκα. Ψάχνοντας ανακάλυψα οτι με το όνομα αυτό είναι μια πόλη στη Ινδία γεννέτηρα του επιφαινόμενου Dattatreya και στο Αρτζεμπαιτζάν σήμαίνει μελωδία.

Το μακάμι ανήκει στη οικογένεια του Rast.

Περισσότερες πληροφορίες σε λίγο.

Links

-   <https://en.wikipedia.org/wiki/Mahur,_Maharashtra>
-   <https://en.wikipedia.org/wiki/Dattatreya>
