+++
title = "Μακάμ Ουσάκ"
date = "2017-09-23"
tags = [ "makam", "theory", "dromoi", "modes", "ussak" ]
topics = [ "" ]
+++

<blockquote class="imgur-embed-pub" lang="en" data-id="a/b1TRN"><a href="//imgur.com/b1TRN">MousikoBostani</a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>

-   [Uşşâk Saz Semâîsi, Neyzen Azîz Dede](http://www.youtube.com/watch?v=fdzfFfthUts) (Signell 1977:39)
