+++
title = "Μακάμ Νεβεσέρ"
date = "2017-09-24"
tags = [ "makam", "dromoi", "modes", "neveser", "theory" ]
topics = [ "" ]
+++

<a href="https://postimages.org/" target="_blank"><img src="https://s26.postimg.org/z5ozwomhl/neveser.png" alt="neveser"/></a>

-   5χορδο Νικρίζ - 4χορδο Χιτζάζ
-   Είσοδος: στο Ράστ ή στο Νεβά
-   Κατάληξη: στο Ραστ
-   Πορεία: Ξεκινάει με το 5χορδο Νικρίζ στο ραστ, συνεχίζει 4χορδο Χιτζάζ στο νεβά και καταλήγει με καθοδική πορεία στο ράστ.
-   Προσαγωγέας: ημιτόνιο
