+++
title = "Πεντάχορδο Μπουσελίκ"
date = "2017-09-24"
tags = [ "pentachord", "buselic" ]
topics = [ "" ]
+++

<a href='https://postimages.org/' target='_blank'><img src='https://s26.postimg.org/poc7tcjq1/buselik.png' border='0' alt='buselik'/></a>

-   Σκάει μύτη ως τετράχορδο αλλα και ως πεντάχορδο στη θέση Ντουγκιάχ
-   έχει προσαγωγέα 5 κόμματα (ημιτόνιo πες εσύ)
-   Όταν είναι απο Ραστ το λένε νιχαβέντ
-   Ο κ. Βούλγαρης αναφέρει κι άλλα στο βιβλίο του και για πιο προχώ θα τους ήταν χρήσιμα
