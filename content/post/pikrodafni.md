+++
title = "Στης Πικροδάφνης"
date = "2017-09-22"
tags = [ "epirus" ]
topics = [ "notopic" ]
+++

<div class="org-center">
<a href="https://picasaweb.google.com/lh/photo/THeBfzBOj6PAKzWDc4BjTdMTjNZETYmyPJy0liipFm0?feat=embedwebsite"><img src="https://lh3.googleusercontent.com/-9rVBxWv7Gu8/V5fGf9CPgOI/AAAAAAAAD4E/YBqkV3b_oskPA8p_7hNvgC0rFlaOEVr0wCCo/s800/Screen%2BShot%2B2016-07-26%2Bat%2B23.22.07.png" height="357" width="350" /></a>
</div>

<p class="verse">
Στης πικροδάφνης τον ανθό<br  />
έγειρα ν’ αποκοιμηθώ<br  />
άιντε λίγο ύπνο για να πάρω<br  />
άιντε κι είδα όνειρο μεγάλο<br  />
<br  />
Παντρεύεται η αγάπη μου<br  />
για πείσμα για γινάτι μου<br  />
και της δίνουν τον εχθρό μου<br  />
για το πείσμα το δικό μου<br  />
<br  />
Στο γάμο τους με προσκαλούν<br  />
και για κουμπάρο με καλούν<br  />
για να πάω να στεφανώσω<br  />
δυο κορμάκια να ενώσω<br  />
<br  />
Περνώ τα στέφανα χρυσά,<br  />
βάστα καημένη μου καρδιά,<br  />
και λαμπάδες απ’ ασήμι,<br  />
έλεος κι ελεημοσύνη.<br  />
(δεν υπάρχει εμπιστοσύνη)<br  />
<br  />
Και τα χερό- κι αϊμάν αϊμάν,<br  />
και τα χεροκρατήματα<br  />
λέει κι αυτά μαργαριτάρι,<br  />
χαρά στο νιο που θα σε πάρει.<br  />
</p>

HGadfsajkfgh
