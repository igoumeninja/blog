+++
title = "Μακάμ Σουλτανί Γιεγκιάχ"
date = "2017-09-23"
tags = [ "makam", "theory", "dromoi", "modes", "sultani-yegah" ]
topics = [ "" ]
+++

<blockquote class="imgur-embed-pub" lang="en" data-id="a/aq2Pv"><a href="//imgur.com/aq2Pv"></a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>

<div id="theme-tagcloud" class="col-sm-12" style="margin-bottom: 15px;">
{{ $tags := $.Site.Taxonomies.tags.ByCount }}
{{ $v1 := where $tags "Count" ">=" 3 }}
{{ $v2 := where $v1 "Term" "not in" (slice "hugo" "tags" "rss") }}
{{ range $v2 }}
{{ if .Term }}
{{ $tagURL := printf "tags/%s" .Term | relURL }}
<a href="{{ $tagURL }}" class="btn btn-default" role="button" style="text-transform: uppercase; font-size: 12px; padding-right: 5px; padding-left: 5px;" >{{ .Term }} <span class="badge">({{ .Count }})</span></a>
{{ end }}
{{ end }}
</div>
