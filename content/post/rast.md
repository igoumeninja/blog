+++
title = "Μακάμ Ραστ"
date = "2017-09-23"
tags = [ "makam", "theory", "rast", "modes", "dromoi" ]
topics = [ "" ]
+++

<blockquote class="imgur-embed-pub" lang="en" data-id="Iqf0kHj"><a href="//imgur.com/Iqf0kHj">View post on imgur.com</a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>

-   Βάση: **Ράστ**
-   Δεσπόζων φθόγος: **Νεβά** *(γίνεται μισή κατάληξη με Ράστ)*
-   Μελωδική πορεία: **Ανιούσα**
-   Προσαγωγέας: **Ιράκ**
-   Δομή: **5χορδο** - **4χορδο**
-   Ονόματα φθόγγων: `Γεγκιάχ, Χουσεϊνί Ασιράν, Ιράκ, Ραστ, Ντουγκιάχ, Σεγκιάχ, Τσαργκιάχ, Νεβά, Χουσεϊνί, Εβίτσ, Γκερντανιέ`

-   Kışları yaz yapan hüner dildedir
    -   <http://www.neyzen.com/nota_arsivi/02_klasik_eserler/075_rast/kislari_yaz_yapan_ney.pdf>
    -   <https://www.facebook.com/video/video.php?v=254113497961752>
-   Rast Medhal Peşrev - Ney Taksimi
    -   <http://www.youtube.com/watch?v=AIzfjXnurD4>
-   Ενδιαφέρον σύνδεσμος:
    -   [Rast makam lesson Ross Daly Labyrinth Crete](http://www.youtube.com/watch?v=1IC9-Ec4hM0)
    -   [www.oudcafe.com](http://www.oudcafe.com/rast_lesson.htm)
-   [Video with score for mansur Ney](http://www.youtube.com/watch?v=npBQLhCDoSw)
-   Συλλογή απο rast με suburde
    -   01. Γιώργος Μπατζάνος (1900-1977) - Το πρώτο ούτι της Ανατολής
    -   02.Najib Coutya - Aleppo qudud <http://www.youtube.com/watch?v=TIEP1n7vqRY>
    -   rast<sub>taksim</sub> (<http://www.kultur.gov.tr/EN,35167/examples-of-ottoman-music.html>)
    -   Ζεμπεκάνο Σπανιόλο (Γιώργος Μπάτης)
    -   Μαρίκα Χασικλού (Βαγγέλης Παπάζογλου)
    -   Γκαμηλιέρικο (Γιώργος Μπάτης)
    -   Αλεξανδινή Φελάχα (Σεμσής - Εσκενάζυ)
    -   Η Μαρίκα η Δασκάλα (Τούντας)
    -   Οι τρείς ορφανές (Ασίκης)
    -   Μες του Βάβουλα τη γούβα (Παγιουμτζής - Κερομύτης)
    -   Τι θελα και σ'αγαπουσα
    -   Ντάρι - ντάρι
    -   Χαρικλάκι.
    -   Καναρίνι
    -   Αλατσατιανή

-   <http://www.youtube.com/watch?v=LkgCqk6Oj40>
