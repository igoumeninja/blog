+++
title = "Μακάμ Χιτζάζ"
date = "2017-09-24"
tags = [ "makam", "theory", "mode", "dromoi", "hicaz" ]
topics = [ "" ]
+++

<a href="https://postimages.org/" target="_blank"><img src="https://s26.postimg.org/8mnz4zu8p/hicaz.png" alt="hicaz"/></a>

-Έργα

-   Πολλά έργα υπάρχουν στο ιστότοπο του neyzen.com (<http://www.neyzen.com/makamlar/hicaz.html>)
-   Hicaz Makamı Çalışması (<http://www.youtube.com/watch?v=5aBCQC0FExE>)
-   Hicaz Mandra (Mandira). Συνθεση του SULTAN Abdulaziz ο οποίος βασίλεψε 25 June 1861 - 30 May 1876 και η μάνα του ήταν Βλάχα. Μιά άλλη σύνθεση του είναι το [Hicazkar Sirto](http://www.youtube.com/watch?v=XPTg_H_rP5E). Παρεπιπτώντος έπεσα σε μια πολύ ωραία συλλογή: [Ottoman Turkish Music from 17th century](http://www.youtube.com/watch?v=--Zztr6nhTE&list=RD02XPTg_H_rP5E)
-   Στη παρτιτούρα υπάρχει ως όνομα συνθέτη του Andon Efendi

links:

1.  <http://www.osmanli700.gen.tr/english/sultans/32index.html>
2.  <http://en.wikipedia.org/wiki/Abd%C3%BClaziz>
3.  <http://en.wikipedia.org/wiki/Vlach>
4.  ![img](//en.wikipedia.org/wiki/File:Constantinople_settings_and_traits_%281926%29-_Epirotes.png)

-   Hicaz İlahi: Nice bir uyursun uyanmaz mısın
    
    | Küçükçekmece Musiki Derneği / Şef: Erol ÜNAL  |                                              |
    | Solistler : Mehmet Kemiksiz - Yunus Balcıoğlu | <http://www.youtube.com/watch?v=zX3AYqG-gmI> |
    |                                               |                                              |

-   Hicaz Sin Yuruk Semai
    -   <span class="timestamp-wrapper"><span class="timestamp">&lt;2013-10-21 Mon&gt; </span></span> - Hicaz Sin Yuruk Semai: Rhythm.play(101,50);
    -   Hicaz Ayin (<http://www.youtube.com/watch?v=WI8HU24A28g>)
