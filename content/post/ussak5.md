+++
title = "Πεντάχορδο Ουσάκ"
date = "2017-09-24"
tags = [ "pentachord", "theory", "ussak" ]
topics = [ "" ]
+++

<a href="https://postimages.org/" target="_blank"><img src="https://s26.postimg.org/yiuokwezd/ousak5.png" alt="ousak5"/></a>

-   Όταν είναι τετράχορδο λέγεται Ουσάκ, όταν είναι πεντάχορδο Χουσεϊνί
-   Εμφανίζεται στη θέση
    -   Ντουγκιάχ<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>
    -   Χουσεινί<sup><a id="fnr.2" class="footref" href="#fn.2">2</a></sup>
    -   Μουχαγιέρ<sup><a id="fnr.3" class="footref" href="#fn.3">3</a></sup>
-   έχει προσαγωγέα τόνου

## Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> Ντουγκιάχ είναι η θέση ένα τόνο πιο ψηλά απο το Ραστ

<sup><a id="fn.2" class="footnum" href="#fnr.2">2</a></sup> Χουσεϊνί είναι αν Ραστ είναι το Ντο τότε η Χουσεϊνί είναι η ΛΑ

<sup><a id="fn.3" class="footnum" href="#fnr.3">3</a></sup> Μουχαγιέρ είναι μια οκτάωα πάνω απο το Ντουγκιάχ
